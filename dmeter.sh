#! /usr/bin/env bash

VERSION=$(cd $(dirname $([ -L $0 ] && readlink -f $0 || echo $0)); git rev-parse --short HEAD 2>/dev/null || echo "version unknown")

## Defaults
LENGTH=32
START='['
END=']'
FILL='='
REMAIN=' '
HEAD=''
MIN=0
PERIOD=1
DELTA_T=0

HELPMSG=$(cat <<EOF
dmeter - Generically monitor an input stream as a meter.

Usage:
 dmeter [options] [--] [command]

 If a command is not supplied, data is read from stdin.
 dmeter expects numerical data input formatted in any of the following ways:
  [data] [min] [max]      explicitly specifying the min and max values,
  [data] [max]            specifying the max value, where the min is 0, or
  [data]                  in which case the max value must be given with -M.

Options:
 [command]                Bash command to run. If omitted, data is read from stdin.
 -l, --length <length>    Length of the meter in characters [default: $LENGTH].
 -s, --start <char>       Character for the start of the meter [default: '$START'].
 -e, --end <char>         Character for the end of the meter [default: '$END'].
 -f, --fill <char>        Character for "filled" meter progress [default: '$FILL'].
 -r, --remains <char>     Character for "unfilled" meter progress [default: '$REMAIN'].
 -H, --head <char>        Character for the "arrowhead" of the meter [default: none].
 -m, --min <minvalue>     Minimum data value [default: $MIN]. Overridden by min value from stdin.
 -M, --max <maxvalue>     Maximum data value. Overridden by max value from stdin.
 -w, --watch              Display indefinitely, refreshing periodically.
 -P, --period <period>    Watch refresh period in seconds [default: $PERIOD] (implies -w).
 -a, --append             Append the numeric value of the data to the meter.
 -t, --eta                Append an estimate of the time remaining (implies -w).
 -p, --percent            Like -a, but appends the percent value.

 -h, --help               Display this help.
 -v, --version            Display version.

EOF
       )

## Parse options
OPTS=`getopt -o hvaptwl:P:s:e:f:r:H:m:M: --long help,verbose,append,percent,eta,watch,length:,period:,start:,end:,fill:,remains:,head:,min:,max: -n 'dmeter' -- "$@"`
if [ $? -ne 0 ]; then echo "(error) Failed parsing options." >&2; exit 1; fi
eval set -- "$OPTS"

while true; do
    case "$1" in
        -h | --help )    echo "$HELPMSG"; exit 0;;
        -v | --version ) echo "dmeter $VERSION"; exit 0;;
        -a | --append )  APPEND=1; shift;;
        -p | --percent ) PERCENT=1; shift;;
        -w | --watch)    WATCH=1; shift;;
        -t | --eta )     SHOW_ETA=1; WATCH=1; shift;;
        -l | --length )  LENGTH="$2"; shift; shift;;
        -P | --period)   PERIOD="$2"; WATCH=1; shift; shift;;
        -s | --start )   START="$2"; shift; shift;;
        -e | --end )     END="$2"; shift; shift;;
        -f | --fill)     FILL="$2"; shift; shift;;
        -r | --remains)  REMAIN="$2"; shift; shift;;
        -H | --head)     HEAD="$2"; shift; shift;;
        -m | --min)      MIN="$2"; shift; shift;;
        -M | --max)      MAX="$2"; shift; shift;;
        -- ) shift; break;;
        * ) break;;
    esac
done

# Command is supplied after args
DATA_CMD="$1"

function clean_exit {
    ## We might hide the cursor earlier, so make sure it's reset
    tput cnorm
    if [ -z "$1" ]; then
        exit 0
    else
        exit "$1"
    fi
}

function fmt_eta {
    ((H=${1} / 3600))
    ((M=(${1} % 3600) / 60))
    ((S=${1} % 60))
    if [ $H -gt 0 ]; then
        printf "%02d:%02d:%02d" $H $M $S
    else
        printf "%02d:%02d" $M $S
    fi
}

function validate {
    ## Validate options

    # Parse data, max, min from input
    case "${#DATA_IN[@]}" in
        0) echo "(error) No data in stream."; clean_exit 2;;
        1) ;;
        2) MAX="${DATA_IN[1]}";;
        *) MIN="${DATA_IN[1]}"; MAX="${DATA_IN[2]}";;
    esac
    DATA="${DATA_IN[0]}"
    if [ -z "$D_ZERO" ]; then
        D_ZERO=$DATA
    fi
    DELTA_X=`bc -l <<< "$D_ZERO - $DATA"`

    # Needs a max data value
    if [ -z "$MAX" ]; then
        echo "(error) Max data value must be supplied (with -M)."
        echo
        echo "$HELPMSG"
        clean_exit 1
    fi

    # max != min
    METER_DIFF=`bc -l <<< "$MAX - $MIN"`
    if [ $METER_DIFF = 0 ]; then
        echo "(error) Max and min data values cannot be the same."
        clean_exit 1
    fi

    # Meter length must be greater than padding
    ((PAD_LEN=${#START} + ${#END} + ${#HEAD} ))
    ((METER_LEN=$LENGTH - $PAD_LEN ))
    if [ $METER_LEN -le 0 ]; then
        echo "(error) Meter length ($LENGTH) must be greater than padding length ($PAD_LEN)."
        clean_exit 1
    fi

    # If arrowhead is longer than the meter (?) drop it
    if [ ${#HEAD} -ge $METER_LEN ]; then HEAD=''; fi

    # Build fill and remain slicing strings
    FILL_SLICE=$(printf "%.0s$FILL" $(seq 1 $METER_LEN))
    REMAIN_SLICE=$(printf "%.0s$REMAIN" $(seq 1 $METER_LEN))
}

function print_meter {
    ## Fetch and handle data
    if [ -z "$DATA_CMD" ]; then
        read -a DATA_IN
    else
        read -ra DATA_IN <<< $(bash -c "$DATA_CMD")
        if [ $? -ne 0 ]; then DATA_ERR=1; fi
    fi

    validate

    # Compute length in characters
    FILL_LEN=`bc <<< "$METER_LEN * ($DATA - $MIN) / $METER_DIFF"`
    # Clamp to meter bounds, making space for the arrowhead
    if [ $FILL_LEN -gt $METER_LEN ]; then FILL_LEN=$METER_LEN; fi
    if [ $FILL_LEN -lt 0 ]; then FILL_LEN=0; fi
    ((REMAIN_LEN=$METER_LEN - $FILL_LEN ))

    ## Build meter
    METER_STR="$START"
    if [ $FILL_LEN -gt 0 ]; then
        METER_STR+=${FILL_SLICE:0:$FILL_LEN}
    fi
    METER_STR+="$HEAD"
    if [ $REMAIN_LEN -gt 0 ]; then
        METER_STR+=${REMAIN_SLICE:0:$REMAIN_LEN}
    fi
    METER_STR+="$END"
    if [ "$APPEND" ]; then
        METER_STR+=" $DATA"
    fi
    if [ "$PERCENT" ]; then
        PCT_STR=`bc <<< "scale=2; 100 * ($DATA - $MIN) / $METER_DIFF"`
        METER_STR+=" $PCT_STR%"
    fi
    if [ "$SHOW_ETA" ] && [ $DELTA_T -gt 0 ]; then
        # ETA is roughly remaining X over dX/dT
        EST_ETA_S=`bc <<< "($MAX - $DATA) * $DELTA_T / $DELTA_X"`
        if [ $EST_ETA_S -lt 0 ]; then
            ((EST_ETA_S=-$EST_ETA_S))
        fi
        ETA_STR=$(fmt_eta $EST_ETA_S)
        METER_STR+=" $ETA_STR remaining"
    fi

    # Pad rest of line with whitespace
    PAD_LEN=$(( $(tput cols) - ${#METER_STR} - 1 ))
    if [ $PAD_LEN -gt 0 ]; then
        METER_STR+=$(printf "%.0s " $(seq 1 $PAD_LEN))
    fi

    echo -n "$METER_STR"
}

## We're done here. Print it
if [ "$WATCH" ]; then

    # Hide cursor, unhide on SIGINT
    tput civis
    trap 'clean_exit 0' INT

    while [ -z "$DATA_ERR" ]; do
        print_meter
        sleep $PERIOD
        ((DELTA_T=$DELTA_T + $PERIOD ))
        echo -ne "\r"
    done
else
    print_meter
    echo
fi

clean_exit 0
